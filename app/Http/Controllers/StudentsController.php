<?php
 
namespace App\Http\Controllers;

use App\Student;

class StudentsController extends Controller
{
    public function index()
    {
    	$title = 'Students List';
    	$students = Student::all();
    	return view('students.students_list', compact('title', 'students'));
    }

    public function show($student_id)
    {
    	$title = 'Student Detail';
    	$student = Student::find($student_id);
    	//return $student;
    	return view('students.students_detail', compact('title', 'student'));
    }
}
